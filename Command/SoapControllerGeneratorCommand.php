<?php

namespace Metropolis\MicroserviceBundle\Command;

use Assert\Assertion;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Yaml\Yaml;

class SoapControllerGeneratorCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('soap:generator:controller')
            ->setDescription('SOAP Controller Generator')
            ->addArgument('wsdl', InputArgument::REQUIRED, 'WSDL for the third party service')

            ->addOption(
                'controller-name',
                null,
                InputOption::VALUE_OPTIONAL,
                'The Controller Name',
                'Default'
            )
            ->addOption(
                'bundle',
                null,
                InputOption::VALUE_OPTIONAL,
                'The Bundle in whicht the SOAP Controller should be generated',
                'AppBundle'
            )
            ->addOption(
                'role',
                null,
                InputOption::VALUE_OPTIONAL,
                'A role (i.e. ROLE_API) to restrict access to the function.'
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $templating = $this->getContainer()->get('templating');
        $kernel = $this->getContainer()->get('kernel');
        $bundleName = $input->getOption('bundle');
        $role = $input->getOption('role');
        $namespace = $kernel->getBundle($bundleName)->getNamespace();

        $wsdl = $input->getArgument('wsdl');
        $controllerName = $input->getOption('controller-name');

        Assertion::url($wsdl,  'The provided wsdl is not a valid url!');
        Assertion::regex($controllerName, '/[A-Za-z0-9]{1,}/i');

        $output->writeln("Getting information from <info>{$wsdl}</info>");
        $soapClient = new \BubbleSOAP($wsdl);

        $output->writeln('Writing WSDL file');
        file_put_contents(
            $kernel->locateResource("@{$bundleName}/Resources/wsdl").'/'.basename($wsdl),
            file_get_contents($wsdl)
        );

        $functionAndParameters = $this->extractFunctionAndParameters($soapClient);

        $controllerTemplate = $templating->render(
            "MetropolisMicroserviceBundle:SoapGenerator:controller.php.twig",
            [
                'bundleName' => $bundleName,
                'namespace' => $namespace,
                'controllerName' => $controllerName,
                'functions' => $functionAndParameters,
                'wsdl' => $wsdl,
                'role' => $role
            ]
        );

        $controllerFile = $kernel->locateResource("@{$bundleName}/Controller")."/{$controllerName}Controller.php";

        if (file_exists($controllerFile)) {
            $output->writeln("<error>File exists. It will be deleted</error>");
            unlink($controllerFile);
        }

        $output->writeln("Writing controller php file to <info>{$controllerFile}</info>");
        file_put_contents($controllerFile, $controllerTemplate);

        // create service definition
        $controllerNameLowered = strtolower($controllerName);
        $bundleNameLowered = substr(strtolower($bundleName), 0, stripos(strtolower($bundleName), 'bundle'));
        $controllerServiceName = "{$bundleNameLowered}.controller.{$controllerNameLowered}_controller";
        $controllerNamespace = $namespace.'\\Controller\\'.$controllerName.'Controller';

        $this->writeToServiceConfigYaml($output, $kernel, $bundleName, $controllerServiceName, $controllerNamespace);
        $this->writeToRoutingYaml($output, $kernel, $bundleName, $controllerNameLowered, $controllerServiceName);
        $this->writeToParametersYaml($output, $controllerServiceName, $wsdl);
    }

    /**
     * @param OutputInterface $output
     * @param KernelInterface $kernel
     * @param $bundleName
     * @param $controllerNameLowered
     * @param $controllerServiceName
     */
    protected function writeToRoutingYaml(
        OutputInterface $output,
        KernelInterface $kernel,
        $bundleName,
        $controllerNameLowered,
        $controllerServiceName
    ) {
        // add parameters
        $routingFile = $kernel->locateResource("@{$bundleName}/Resources/config/routing.yml");
        $routingConfig = file_get_contents($routingFile);

        $routingConfigTemplate = <<<YAML


{$controllerNameLowered}:
    type: rest
    resource: {$controllerServiceName}
YAML;

        if (stripos($routingConfig, $controllerNameLowered) === false) {
            $output->writeln("Writing routes to routing file <info>{$routingFile}</info>");
            file_put_contents($routingFile, $routingConfigTemplate, FILE_APPEND);
        } else {
            $output->writeln("Routing file not written because it already exists in the file.");
            $output->writeln($routingConfigTemplate);
        }
    }

    /**
     * @param OutputInterface $output
     * @param $controllerServiceName
     * @param $wsdl
     */
    protected function writeToParametersYaml(
        OutputInterface $output,
        $controllerServiceName,
        $wsdl
    ) {
        foreach (['parameters.yml', 'parameters.yml.dist'] as $parameterFileName) {
            $parametersFile = $this->getContainer()->getParameter('kernel.root_dir').'/config/'.$parameterFileName;
            $parameters = Yaml::parse(file_get_contents($parametersFile));
            $parameterName = $controllerServiceName.'.wsdl';

            if (empty($parameters['parameters'][$parameterName])) {
                $output->writeln("Writing parameters to <info>parameters.yml</info> file");
                $parameters['parameters'][$parameterName] = $wsdl;
                file_put_contents($parametersFile, Yaml::dump($parameters));
            }
        }
    }

    /**
     * @param OutputInterface $output
     * @param $kernel
     * @param $bundleName
     * @param $controllerServiceName
     * @param $controllerNamespace
     */
    protected function writeToServiceConfigYaml(
        OutputInterface $output,
        KernelInterface $kernel,
        $bundleName,
        $controllerServiceName,
        $controllerNamespace
    ) {
        $serviceFile = $kernel->locateResource("@{$bundleName}/Resources/services/controllers.yml");
        $serviceConfig = file_get_contents($serviceFile);
        $serviceConfigTemplate = <<<YAML


  {$controllerServiceName}:
    class: {$controllerNamespace}
    arguments:
      - "%{$controllerServiceName}.wsdl%"
      - @logger
      - @service_container
YAML;
        if (stripos($serviceConfig, $controllerServiceName) === false) {
            $output->writeln("Writing service definition to service file <info>{$serviceFile}</info>");
            file_put_contents($serviceFile, $serviceConfigTemplate, FILE_APPEND);
        } else {
            $output->writeln("Routing file not written because it already exists in the file.");
            $output->writeln($serviceConfigTemplate);
        }
    }

    /**
     * @param \BubbleSOAP $soapClient
     * @return array
     */
    protected function extractFunctionAndParameters(\BubbleSOAP $soapClient)
    {
        $functionAndParameters = [];
        foreach ($soapClient->__getFunctionsNames() as $functionsName) {
            $paramList = (array)$soapClient->__getType($functionsName);
            $parameters = array_keys($paramList);

            foreach ($parameters as $num => $param) {
                if (is_integer($param)) {
                    unset ($parameters[$num]);
                }
            }

            $functionAndParameters[$functionsName] = $parameters;
        }
        return $functionAndParameters;
    }
}
